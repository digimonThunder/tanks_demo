﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField]
    private FixedJoystick m_MovePositionJoystick;

    [SerializeField]
    private FixedJoystick m_RotationDirectionJoystick;

    [SerializeField]
    private Text m_PlayerOneHealth;

    [SerializeField]
    private Text m_PlayerTwoHealth;

    GameEventManager mGameEventManager;


    void Awake()
    {
        mGameEventManager = GameEventManager.SharedInstance();


        mGameEventManager.EventOnPlayerHealthReduced += OnPlayerHealthReduced;
    }

    void FixedUpdate()
    {
        mGameEventManager.TriggerEventOnMoveJoystickValueChanged(m_MovePositionJoystick.Horizontal);

        mGameEventManager.TriggerEventOnRotateJoystickValueChanged(m_RotationDirectionJoystick.Horizontal);

    }

    public void OnFireClicked()
    {
        mGameEventManager.TriggerEventOnFireClicked();
    }

    void OnPlayerHealthReduced(int inPlayerId, float inValue)
    {
        switch (inPlayerId)
        {
            case 1:

                m_PlayerOneHealth.text = inValue.ToString();

                break;

            case 2:

                m_PlayerTwoHealth.text = inValue.ToString();

                break;
        }
    }


}
