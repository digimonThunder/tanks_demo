﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[Serializable]
public class TerrainBlock
{
    public TerrainBlockState mTerrainBlock;

    public Sprite mTerrainBlockSprite;
}


public class TerrainBlockBase : TileBase
{
    public TerrainBlock[] m_TerrainBlocks;

    private TerrainBlockState mBlockState = TerrainBlockState.MidFull;

    public TerrainBlockState CurrentBlockState
    {
        get
        {
            return mBlockState;
        }

        set
        {
            mBlockState = value;
        }
    }

    public override void GetTileData(Vector3Int location, ITilemap tilemap, ref TileData tileData)
    {
        tileData.sprite = GetSprite();

        tileData.colliderType = Tile.ColliderType.Sprite;
    }

    private Sprite GetSprite()
    {
        Sprite retVal = null;

        switch (mBlockState)
        {
            case TerrainBlockState.Left:

                retVal = m_TerrainBlocks[1].mTerrainBlockSprite;

                break;

            case TerrainBlockState.Right:

                retVal = m_TerrainBlocks[0].mTerrainBlockSprite;

                break;

            case TerrainBlockState.MidFull:

                retVal = m_TerrainBlocks[2].mTerrainBlockSprite;

                break;

            case TerrainBlockState.TopFull:

                retVal = m_TerrainBlocks[3].mTerrainBlockSprite;

                break;
        }

        return retVal;
    }


#if UNITY_EDITOR
    // The following is a helper that adds a menu item to create a RoadTile Asset
    [UnityEditor.MenuItem("Assets/Create/TerrainBlock")]
    public static void CreateRoadTile()
    {
        string path = UnityEditor.EditorUtility.SaveFilePanelInProject("Save Terrain Block", "New Terrain Tile", "Asset", "Save Terrain Tile", "Assets");
        if (path == "")
            return;
        UnityEditor.AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<TerrainBlockBase>(), path);
    }
#endif
}
