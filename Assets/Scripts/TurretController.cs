﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TurretController : MonoBehaviour
{
    [SerializeField]
    private Transform m_BarrelTop;

    [SerializeField]
    private Transform m_Barrel;

    [SerializeField]
    public Rigidbody2D m_Bullet;

    [SerializeField]
    private SpriteRenderer m_Turret;

    private float mBulletSpeed = 600f;

    private float mMaxHealth;

    private bool mPlayerTurn;

    private int mId;

    private Vector3 mLeftMaxPosition;

    private Vector3 mRightMaxPosition;


    public bool PlayerTurn
    {
        set
        {
            mPlayerTurn = value;
        }
    }


    // Update is called once per frame
    void Awake()
    {
        GameEventManager.SharedInstance().EventOnFireClicked += Fire;

        GameEventManager.SharedInstance().EventOnMoveJoystickValueChanged += OnHorizontalMoveJoystickValueChanged;

        GameEventManager.SharedInstance().EventOnRotateJoystickValueChanged += OnRotateJoystickValueChanged;

        Camera cam = Camera.main;

        float height = 2f * cam.orthographicSize;

        float mWidth = (height * cam.aspect) / 2;

        mLeftMaxPosition = new Vector3(-mWidth,transform.position.y,transform.position.z);

        mRightMaxPosition = new Vector3(mWidth,transform.position.y,transform.position.z);
    }

    public void SetUpValues(int inId,float inMaxHealth,Sprite inSprite)
    {
        mId = inId;

        mMaxHealth = inMaxHealth;

        m_Turret.sprite = inSprite;

        GameEventManager.SharedInstance().TriggerEventOnPlayerHealthReduced(mId,mMaxHealth);

        if(mId == 1)
        {
            m_Barrel.rotation = Quaternion.Euler(0,0,-45);
        }
        else
        {
            m_Barrel.rotation = Quaternion.Euler(0,0,45);
        }

    }

    void OnHorizontalMoveJoystickValueChanged(float inValue)
    {
        if (mPlayerTurn)
        {
            float newPos = inValue * 10 * Time.deltaTime;

            transform.Translate(newPos, 0, 0);

        }
    }

    void OnRotateJoystickValueChanged(float inValue)
    {
        if (mPlayerTurn)
        {
            float newAngle = inValue * 100 * Time.deltaTime * -1;

            m_Barrel.Rotate(Vector3.forward * newAngle);
        }
    }

    void Update()
    {
        transform.position = transform.position.x < mLeftMaxPosition.x ? mLeftMaxPosition : transform.position;

        transform.position = transform.position.x > mRightMaxPosition.x ? mRightMaxPosition : transform.position;
    }


    void OnCollisionEnter2D(Collision2D collision)
    {
        {
            Vector3 hitPosition = Vector3.zero;

            if ("Bullet" == collision.gameObject.tag)
            {
                mMaxHealth -= 5;

                // Debug.Log("Health Reduced to " + mMaxHealth);

                GameEventManager.SharedInstance().TriggerEventOnPlayerHealthReduced(mId,mMaxHealth);

                StartCoroutine(TimedDestory(collision.gameObject));
            }
        }
    }

    IEnumerator TimedDestory(GameObject inObject)
    {
        yield return new WaitForSeconds(5);

        Destroy(inObject);
    }


    void Fire()
    {
        if (mPlayerTurn)
        {
            mPlayerTurn = false;

            var firedBullet = Instantiate(m_Bullet, m_BarrelTop.position, m_BarrelTop.rotation);

            firedBullet.AddForce(m_BarrelTop.up * mBulletSpeed);
        }
    }

}
