﻿using UnityEngine;

public class GameEventManager
{
    public delegate void GameEvent();

    public delegate void GameEventFloat(float inValue);

    public delegate void GameEventInt(int inValue);

    public delegate void GameEventBool(bool inValue);

    public delegate void GameEventString(string inValue);

    public delegate void BulletHitTerrain(Vector3Int inLoc, TerrainBlockBase inTile, TerrainBlockState inBlockState);


    public delegate void GameEventPlayerHealth(int inId,float inHealth);

    //-----------------------
    public event BulletHitTerrain EventOnBulletHitTerrain;

    public event GameEventFloat EventOnMoveJoystickValueChanged,EventOnRotateJoystickValueChanged;

    public event GameEvent EventOnFireClicked,EventSwitchControlToOtherPlayer;

    public event GameEventPlayerHealth EventOnPlayerHealthReduced;

    //-----------------------

    private static GameEventManager sGlobalRef = null;

    public static GameEventManager SharedInstance()
    {
        if (sGlobalRef == null)
        {
            sGlobalRef = new GameEventManager();
        }

        return sGlobalRef;
    }

    private GameEventManager()
    {

    }

    public void TriggerBulletHitTerrain(Vector3Int inLoc, TerrainBlockBase inTile, TerrainBlockState inBlockState)
    {
        if (EventOnBulletHitTerrain != null)
        {
            EventOnBulletHitTerrain(inLoc, inTile, inBlockState);
        }
    }

    public void TriggerEventOnMoveJoystickValueChanged(float inValue)
    {
        if (EventOnMoveJoystickValueChanged != null)
        {
            EventOnMoveJoystickValueChanged(inValue);
        }
    }

     public void TriggerEventOnRotateJoystickValueChanged(float inValue)
    {
        if (EventOnRotateJoystickValueChanged != null)
        {
            EventOnRotateJoystickValueChanged(inValue);
        }
    }

    public void TriggerEventOnFireClicked()
    {
         if (EventOnFireClicked != null)
        {
            EventOnFireClicked();
        }
    }

    public void TriggerEventSwitchControlToOtherPlayer()
    {
         if (EventSwitchControlToOtherPlayer != null)
        {
            EventSwitchControlToOtherPlayer();
        }
    }

    public void TriggerEventOnPlayerHealthReduced(int inID,float inVal)
    {
         if (EventOnPlayerHealthReduced != null)
        {
            EventOnPlayerHealthReduced(inID,inVal);
        }
    }

}
