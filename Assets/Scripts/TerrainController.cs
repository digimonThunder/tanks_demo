﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;


public enum TerrainBlockState
{
    TopFull,
    MidFull,
    Left,
    Right,
    None
}

public class TerrainController : MonoBehaviour
{
    [SerializeField]
    private Tilemap m_TileMap;

    [SerializeField]
    private TileBase m_TileBase;

    private TerrainBlockState[,] mTerrainBlockGrid;

    private int mMaxTerrianRow = 0;

    private int mMaxTerrianCol = 0;


    public Tilemap Tilemap
    {
        get
        {
            return m_TileMap;
        }
    }

    void Awake()
    {
        GameEventManager.SharedInstance().EventOnBulletHitTerrain += OnBulletHitTerrain;
    }

    public void GeneratePlatform()
    {
        if (m_TileMap != null)
        {
            Camera cam = Camera.main;

            float height = 2f * cam.orthographicSize;

            float width = height * cam.aspect;

            mMaxTerrianRow = Mathf.CeilToInt(width * 0.65f);

            mMaxTerrianCol = Mathf.CeilToInt(height * 0.65f);

            mTerrainBlockGrid = new TerrainBlockState[2 * mMaxTerrianRow, mMaxTerrianCol];

            for (int x = -mMaxTerrianRow; x < mMaxTerrianRow; x++)
            {
                for (int y = 0; y > -mMaxTerrianCol; y--)
                {
                    Vector3Int pos = new Vector3Int(x, y, 0);

                    m_TileMap.SetTile(pos, m_TileBase);

                    TerrainBlockBase tile = m_TileMap.GetTile<TerrainBlockBase>(pos);

                    TerrainBlockState state = pos.y == 0 ? TerrainBlockState.TopFull : TerrainBlockState.MidFull;

                    int indexX = mMaxTerrianRow + x;

                    int indexY = mMaxTerrianCol + y - 1;

                    mTerrainBlockGrid[indexX, indexY] = state;

                    tile.CurrentBlockState = state;

                    m_TileMap.RefreshTile(pos);

                }
            }
        }

    }


    private void UpdateBlockStateAtPosition(Vector3Int inTilePos, TerrainBlockState inState)
    {
        if (mTerrainBlockGrid != null)
        {
            int indexX = mMaxTerrianRow + inTilePos.x;

            int indexY = mMaxTerrianCol + inTilePos.y - 1;

            // Debug.Log(" index X  " + indexX + " index Y " + indexY + " State " + inState);

            mTerrainBlockGrid[indexX, indexY] = inState;
        }
    }

    private TerrainBlockState GetCurrentBlockState(Vector3Int inTilePos)
    {
        TerrainBlockState retVal = TerrainBlockState.None;

        if (mTerrainBlockGrid != null)
        {
            int indexX = mMaxTerrianRow + inTilePos.x;

            int indexY = mMaxTerrianCol + inTilePos.y - 1;

            try { retVal = mTerrainBlockGrid[indexX, indexY]; } catch { }
        }
        return retVal;
    }


    private void ChangeBlockState(Vector3Int position, TerrainBlockBase inTile, TerrainBlockState inState)
    {
        if (m_TileMap != null && inTile != null)
        {
            TerrainBlockState currentState = GetCurrentBlockState(position);

            if (currentState != TerrainBlockState.None)
            {
                UpdateBlockStateAtPosition(position, inState);

                inTile.CurrentBlockState = inState;

                m_TileMap.RefreshTile(position);
            }
        }
    }


    private void RefreshSurroundingBlocks(Vector3Int position, TerrainBlockBase inTile)
    {
        if (m_TileMap != null && inTile != null)
        {
            Vector3Int locationLeft = new Vector3Int(position.x - 1, position.y, position.z);
            Vector3Int locationRight = new Vector3Int(position.x + 1, position.y, position.z);
            Vector3Int locationTop = new Vector3Int(position.x, position.y + 1, position.z);
            Vector3Int locationDown = new Vector3Int(position.x, position.y - 1, position.z);

            TerrainBlockBase leftTile = m_TileMap.GetTile<TerrainBlockBase>(locationLeft);
            TerrainBlockBase rightTile = m_TileMap.GetTile<TerrainBlockBase>(locationRight);
            TerrainBlockBase topTile = m_TileMap.GetTile<TerrainBlockBase>(locationTop);
            TerrainBlockBase downTile = m_TileMap.GetTile<TerrainBlockBase>(locationDown);

            if (inTile != null)
            {
                switch (inTile.CurrentBlockState)
                {
                    case TerrainBlockState.TopFull:
                    case TerrainBlockState.MidFull:

                        break;


                    case TerrainBlockState.None:

                        if (leftTile != null)
                        {
                            TerrainBlockState currentState = GetCurrentBlockState(locationLeft);

                            if (currentState == TerrainBlockState.Left)
                            {
                                ChangeBlockState(locationLeft, leftTile, TerrainBlockState.None);
                            }
                            else
                            {
                                ChangeBlockState(locationLeft, leftTile, TerrainBlockState.Left);

                                CheckUpAndDownBlock(locationLeft,leftTile);
                            }


                        }

                        if (rightTile != null)
                        {
                            TerrainBlockState currentState = GetCurrentBlockState(locationLeft);

                            ChangeBlockState(locationRight, rightTile, TerrainBlockState.Right);


                            if (currentState == TerrainBlockState.Right)
                            {
                                ChangeBlockState(locationRight, rightTile, TerrainBlockState.None);
                            }
                            else
                            {
                                ChangeBlockState(locationRight, rightTile, TerrainBlockState.Right);

                                CheckUpAndDownBlock(locationRight,rightTile);

                            }


                        }

                        if (downTile != null)
                        {
                            ChangeBlockState(locationDown, downTile, TerrainBlockState.TopFull);
                        }

                        break;


                    case TerrainBlockState.Left:
                    case TerrainBlockState.Right:

                        if (topTile != null)
                        {
                            ChangeBlockState(locationTop, topTile, TerrainBlockState.None);
                        }

                        break;
                }
            }
        }


    }

    private void CheckUpAndDownBlock(Vector3Int position, TerrainBlockBase inTile)
    {
        if (m_TileMap != null && inTile != null)
        {
            Vector3Int locationTop = new Vector3Int(position.x, position.y + 1, position.z);
            Vector3Int locationDown = new Vector3Int(position.x, position.y - 1, position.z);

            TerrainBlockBase topTile = m_TileMap.GetTile<TerrainBlockBase>(locationTop);
            TerrainBlockBase downTile = m_TileMap.GetTile<TerrainBlockBase>(locationDown);

            if (inTile != null)
            {
                switch (inTile.CurrentBlockState)
                {
                    case TerrainBlockState.Left:
                    case TerrainBlockState.Right:

                        if (topTile != null)
                        {
                            ChangeBlockState(locationTop, topTile, TerrainBlockState.None);
                        }

                        break;
                }
            }
        }

    }

    public void OnBulletHitTerrain(Vector3Int inLoc, TerrainBlockBase inTile, TerrainBlockState inBlockState)
    {
        ChangeBlockState(inLoc, inTile, inBlockState);

        RefreshSurroundingBlocks(inLoc, inTile);
    }
}
