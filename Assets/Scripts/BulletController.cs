﻿using UnityEngine;
using UnityEngine.Tilemaps;

public class BulletController : MonoBehaviour
{
    private TerrainController m_TerrainController;

    void Awake()
    {
        m_TerrainController = FindObjectOfType<TerrainController>();
    }


    void OnCollisionEnter2D(Collision2D collision)
    {
        Vector3 hitPosition = Vector3.zero;

        if (m_TerrainController.Tilemap != null && m_TerrainController.Tilemap.gameObject == collision.gameObject)
        {
            foreach (ContactPoint2D hit in collision.contacts)
            { 
                hitPosition.x = hit.point.x - 0.01f * hit.normal.x;
                hitPosition.y = hit.point.y - 0.01f * hit.normal.y;

                Vector3Int pos = m_TerrainController.Tilemap.WorldToCell(hitPosition);

                TerrainBlockBase tile = m_TerrainController.Tilemap.GetTile<TerrainBlockBase>(pos);

                GameEventManager.SharedInstance().TriggerBulletHitTerrain(pos,tile,TerrainBlockState.None);

                Destroy(gameObject);

                break;

            }

            GameEventManager.SharedInstance().TriggerEventSwitchControlToOtherPlayer();

        }
    }


}