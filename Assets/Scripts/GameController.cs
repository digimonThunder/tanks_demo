﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private TerrainController m_TerrainController;

    [SerializeField]
    private TurretController m_TurretPrefab;


    [SerializeField]
    private Sprite[] m_PlayerSprites;

    private TurretController mPlayerOne;

    private TurretController mPlayerTwo;


    private TurretController mCurrentPlayer;


    // Start is called before the first frame update
    void Start()
    {
        GameEventManager.SharedInstance().EventSwitchControlToOtherPlayer += OnSwitchControlToOtherPlayer;

        m_TerrainController.GeneratePlatform();

        SetUpPlayers();
    }


    void SetUpPlayers()
    {
        mPlayerOne = Instantiate(m_TurretPrefab,new Vector3(-10,5,0),Quaternion.identity).GetComponent<TurretController>();

        mPlayerTwo = Instantiate(m_TurretPrefab,new Vector3(10,5,0),Quaternion.identity).GetComponent<TurretController>();

        mPlayerOne.SetUpValues(1,500,m_PlayerSprites[0]);

        mPlayerTwo.SetUpValues(2,500,m_PlayerSprites[1]);

        mPlayerOne.PlayerTurn = true;

        mCurrentPlayer = mPlayerOne;
    }

    void OnSwitchControlToOtherPlayer()
    {
        mCurrentPlayer.PlayerTurn = false;

        if(mCurrentPlayer == mPlayerOne)
        {
            mCurrentPlayer = mPlayerTwo;
        }
        else 
        {
            mCurrentPlayer = mPlayerOne;
        }

        mCurrentPlayer.PlayerTurn = true;

    }

}
